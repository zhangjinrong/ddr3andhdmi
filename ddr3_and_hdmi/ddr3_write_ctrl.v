
module ddr3_write_ctrl(
input 	wire 		sclk		,
input 	wire 		rst_n		,

input	wire		wr_en		,//由arbit输入，表示可以写
input 	wire          	app_rdy		,
input   wire          	app_wdf_rdy	,

input 	wire 		wr_cmd_req_in	,//fifo传进，有写需求,只起连接作用
input 	wire[27:0] 	wr_cmd_addr	,//fifo传进，写地址（64突发的首地址）
input 	wire[127:0]	wr_data		,//fifo传进，写数据
output  reg 		wr_cmd_en	,//输出到fifo，读命令
output 	wire 		data_req	,//输出到fifo，读数据

output 	reg[27:0]       wr_app_addr	,//输出写地址，64突发，每次+8
output 	reg[2:0]        wr_app_cmd	,//输出写命令，0
output  reg          	wr_app_en	,//输出写使能，与app_rdy配合使用

/****************************************************************/

output	wire		wr_cmd_req_out	,//传给arbit，只起连接作用
output  wire[127:0]     app_wdf_data	,//输出写数据，主要起连接作用，不缓存
output  reg          	app_wdf_end	,//输出数据和app_wdf_wren配合使用
output 	reg[15:0]       app_wdf_mask	,//输出掩码，为0
output  reg          	app_wdf_wren	 //输出数据和app_wdf_end配合使用

);

assign 	app_wdf_data=wr_data;
assign	wr_cmd_req_out=wr_cmd_req_in;
assign 	data_req = app_wdf_rdy&app_wdf_wren;

reg[7:0] 	data_cnt 	;
reg 		wr_en1		;
reg[7:0]	cmd_cnt		;

//-----------------------------------------cmd  begin------------------------------------------

//wr_en1
always @ (posedge sclk)
	if(rst_n==0)
		wr_en1<=0;
	else 
		wr_en1<=wr_en;
		
//wr_cmd_en
always @ (posedge sclk)
	if(rst_n==0)
		wr_cmd_en<=0;
	else 
		wr_cmd_en<=wr_en1;
		
//wr_app_en
always @ (posedge sclk)
	if(rst_n == 0)
		wr_app_en<=0;
	else if(wr_en1==1)
		wr_app_en<=1;
	else if((cmd_cnt==63)&&(app_rdy==1))
		wr_app_en<=0;
		
//cmd_cnt
always @ (posedge sclk)
	if(rst_n == 0)
		cmd_cnt<=0;
	else if((cmd_cnt==63)&&(app_rdy==1))
		cmd_cnt<=0;
	else if(app_rdy&&wr_app_en)
		cmd_cnt<=cmd_cnt+1;
		
//wr_app_addr
always @ (posedge sclk)
	if(rst_n == 0)
		wr_app_addr<=0;
	else if(wr_en==1)
		wr_app_addr<=wr_cmd_addr;
	else if(app_rdy&&wr_app_en)
		wr_app_addr<=wr_app_addr+8;
		
//wr_app_cmd
always @ (posedge sclk)
	if(rst_n == 0)
		wr_app_cmd<=0;

//-----------------------------------------cmd  end------------------------------------------

//-----------------------------------------data  begin------------------------------------------
//data_cnt
always @ (posedge sclk)
	if(rst_n == 0)
		data_cnt<=0;
	else if((data_cnt==63)&&(app_wdf_rdy==1))
		data_cnt<=0;
	else if(app_wdf_rdy&&app_wdf_wren)
		data_cnt<=data_cnt+1;
	
	
//app_wdf_mask
always @ (posedge sclk)
	if(rst_n == 0)
		app_wdf_mask<=0;
		
//app_wdf_end,app_wdf_wren
always @ (posedge sclk)
	if(rst_n == 0)
		begin
			app_wdf_end<=0;
			app_wdf_wren<=0;
		end
	else if(wr_en==1)
		begin
			app_wdf_end<=1;
			app_wdf_wren<=1;
		end
	else if((data_cnt==63)&&(app_wdf_rdy==1))
		begin
			app_wdf_end<=0;
			app_wdf_wren<=0;
		end
//-----------------------------------------data  end------------------------------------------

endmodule
