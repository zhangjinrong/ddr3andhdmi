module rx(
input wire clk,
input wire rst_n,
input wire rx,
output reg[7:0] po_data_out,
output reg en_out
);

reg rx1;
reg rx2;
reg flag;
reg cnt_flag;
reg [3:0] cnt_bit; 
reg [12:0] cnt;
reg po_data_flag;
reg [7:0]po_data;
parameter		CNT	=	434;//405;//5207;//433;
/*
ila_0 ila_0_rx1 (
.clk(clk), // input wire clk
.probe0({1'b0,po_data,en_out}) // input wire [9:0] probe0
);
*/


//en_out
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		en_out<=0;
	else if(po_data_flag==1)
		en_out<=1;
	else
		en_out<=0;
//po_data_out
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		po_data_out<=0;
	else if(po_data_flag==1)
		po_data_out<=po_data;	
//po_data_flag
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		po_data_flag<=0;
	else if(cnt_bit==8&&cnt_flag==1)
		po_data_flag<=1;
	else
		po_data_flag<=0;
//po_data
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		po_data<=0;
	else if(cnt_flag==1&&cnt_bit>=1&&cnt_bit<=8)
		po_data<={rx2,po_data[7:1]};
		//po_data<={po_data[6:0],rx2};
	else if(cnt_bit>8&&cnt_bit==0)
		po_data<=0;
//cnt
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		cnt<=0;
	else if(flag==1&&cnt==CNT)
		cnt<=0;
	else if(flag==1&&cnt<CNT)
		cnt<=cnt+1;
	else if(flag==0)
		cnt<=0;
//cnt_bit
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		cnt_bit<=0;
	else if(cnt_flag==1&&flag==1)
		cnt_bit<=cnt_bit+1;
	else if(flag==0)
		cnt_bit<=0;
//cnt_flag
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		cnt_flag<=0;
	else if((cnt==(CNT/2))&&(flag==1))
//else if(cnt==202&&flag==1)
		cnt_flag<=1;
	else
		cnt_flag<=0;
//flag
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		flag<=0;
	else if(cnt_bit==9&&cnt_flag==1)
		flag<=0;
	else if(rx2==0)
		flag<=1;		
//rx1
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		rx1<=1;
	else 
		rx1<=rx;		
//rx2
always @ (posedge clk or negedge rst_n)
	if(rst_n==0)
		rx2<=1;
	else 
		rx2<=rx1;
endmodule