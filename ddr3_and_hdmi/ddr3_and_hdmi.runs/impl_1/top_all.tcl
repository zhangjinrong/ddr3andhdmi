proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set ACTIVE_STEP init_design
set rc [catch {
  create_msg_db init_design.pb
  set_property design_mode GateLvl [current_fileset]
  set_param project.singleFileAddWarning.threshold 0
  set_property webtalk.parent_dir C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.cache/wt [current_project]
  set_property parent.project_path C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.xpr [current_project]
  set_property ip_output_repo C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.cache/ip [current_project]
  set_property ip_cache_permissions {read write} [current_project]
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  add_files -quiet C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.runs/synth_1/top_all.dcp
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_tx_ctrl/fifo_tx_ctrl.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_tx_ctrl/fifo_tx_ctrl.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/ila_2.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/ila_2.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_3.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_3.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235.dcp]
  add_files -quiet c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2.dcp
  set_property netlist_only true [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2.dcp]
  read_xdc -ref fifo_time2 -cells U0 c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2/fifo_time2.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2/fifo_time2.xdc]
  read_xdc -ref ddr3 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ddr3/ddr3/user_design/constraints/ddr3.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ddr3/ddr3/user_design/constraints/ddr3.xdc]
  read_xdc -ref fifo_tx_ctrl -cells U0 c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_tx_ctrl/fifo_tx_ctrl/fifo_tx_ctrl.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_tx_ctrl/fifo_tx_ctrl/fifo_tx_ctrl.xdc]
  read_xdc -prop_thru_buffers -ref rxclk_90 -cells inst c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90_board.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90_board.xdc]
  read_xdc -ref rxclk_90 -cells inst c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/rxclk_90/rxclk_90.xdc]
  read_xdc -ref data_fifo -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo/data_fifo.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo/data_fifo.xdc]
  read_xdc -ref cmd_fifo -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo/cmd_fifo.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo/cmd_fifo.xdc]
  read_xdc -ref ila_2 -cells inst c:/Users/r/Desktop/fpga_autumn/rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/ila_v6_2/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/ila_v6_2/constraints/ila.xdc]
  read_xdc -mode out_of_context -ref ila_3 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_3_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_3_ooc.xdc]
  read_xdc -ref ila_3 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_v6_2/constraints/ila.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/ila_3/ila_v6_2/constraints/ila.xdc]
  read_xdc -mode out_of_context -ref clk50_125 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125_ooc.xdc]
  read_xdc -prop_thru_buffers -ref clk50_125 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125_board.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125_board.xdc]
  read_xdc -ref clk50_125 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_125/clk50_125.xdc]
  read_xdc -mode out_of_context -ref clk50_50 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50_ooc.xdc]
  read_xdc -prop_thru_buffers -ref clk50_50 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50_board.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50_board.xdc]
  read_xdc -ref clk50_50 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk50_50/clk50_50.xdc]
  read_xdc -mode out_of_context -ref clk_65_235 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_ooc.xdc]
  read_xdc -prop_thru_buffers -ref clk_65_235 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_board.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_board.xdc]
  read_xdc -ref clk_65_235 -cells inst c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235.xdc]
  read_xdc -mode out_of_context -ref fifo_tx_mid2 -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2_ooc.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2_ooc.xdc]
  read_xdc -ref fifo_tx_mid2 -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2/fifo_tx_mid2.xdc
  set_property processing_order EARLY [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/fifo_tx_mid2/fifo_tx_mid2/fifo_tx_mid2.xdc]
  read_xdc C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/constrs_1/new/uart.xdc
  read_xdc -ref fifo_time2 -cells U0 c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2/fifo_time2_clocks.xdc
  set_property processing_order LATE [get_files c:/Users/r/Desktop/fpga_autumn/rgmii_mac2/rgmii_mac2/rgmii_mac2.srcs/sources_1/ip/fifo_time2/fifo_time2/fifo_time2_clocks.xdc]
  read_xdc -ref data_fifo -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo/data_fifo_clocks.xdc
  set_property processing_order LATE [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/data_fifo/data_fifo/data_fifo_clocks.xdc]
  read_xdc -ref cmd_fifo -cells U0 c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo/cmd_fifo_clocks.xdc
  set_property processing_order LATE [get_files c:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/cmd_fifo/cmd_fifo/cmd_fifo_clocks.xdc]
  link_design -top top_all -part xc7a35tfgg484-2
  write_hwdef -file top_all.hwdef
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
  unset ACTIVE_STEP 
}

start_step opt_design
set ACTIVE_STEP opt_design
set rc [catch {
  create_msg_db opt_design.pb
  opt_design 
  write_checkpoint -force top_all_opt.dcp
  catch { report_drc -file top_all_drc_opted.rpt }
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
  unset ACTIVE_STEP 
}

start_step place_design
set ACTIVE_STEP place_design
set rc [catch {
  create_msg_db place_design.pb
  implement_debug_core 
  place_design 
  write_checkpoint -force top_all_placed.dcp
  catch { report_io -file top_all_io_placed.rpt }
  catch { report_utilization -file top_all_utilization_placed.rpt -pb top_all_utilization_placed.pb }
  catch { report_control_sets -verbose -file top_all_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
  unset ACTIVE_STEP 
}

start_step route_design
set ACTIVE_STEP route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force top_all_routed.dcp
  catch { report_drc -file top_all_drc_routed.rpt -pb top_all_drc_routed.pb -rpx top_all_drc_routed.rpx }
  catch { report_methodology -file top_all_methodology_drc_routed.rpt -rpx top_all_methodology_drc_routed.rpx }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file top_all_timing_summary_routed.rpt -rpx top_all_timing_summary_routed.rpx }
  catch { report_power -file top_all_power_routed.rpt -pb top_all_power_summary_routed.pb -rpx top_all_power_routed.rpx }
  catch { report_route_status -file top_all_route_status.rpt -pb top_all_route_status.pb }
  catch { report_clock_utilization -file top_all_clock_utilization_routed.rpt }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  write_checkpoint -force top_all_routed_error.dcp
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
  unset ACTIVE_STEP 
}

start_step write_bitstream
set ACTIVE_STEP write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
  catch { write_mem_info -force top_all.mmi }
  write_bitstream -force -no_partial_bitfile top_all.bit 
  catch { write_sysdef -hwdef top_all.hwdef -bitfile top_all.bit -meminfo top_all.mmi -file top_all.sysdef }
  catch {write_debug_probes -quiet -force debug_nets}
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
  unset ACTIVE_STEP 
}

