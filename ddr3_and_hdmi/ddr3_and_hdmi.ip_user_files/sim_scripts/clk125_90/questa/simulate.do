onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib clk125_90_opt

do {wave.do}

view wave
view structure
view signals

do {clk125_90.udo}

run -all

quit -force
