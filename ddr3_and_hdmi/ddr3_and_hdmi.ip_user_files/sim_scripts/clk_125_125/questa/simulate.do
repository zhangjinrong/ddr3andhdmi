onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib clk_125_125_opt

do {wave.do}

view wave
view structure
view signals

do {clk_125_125.udo}

run -all

quit -force
