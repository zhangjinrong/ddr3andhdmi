onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib rxclk_90_opt

do {wave.do}

view wave
view structure
view signals

do {rxclk_90.udo}

run -all

quit -force
