onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib clk_200_opt

do {wave.do}

view wave
view structure
view signals

do {clk_200.udo}

run -all

quit -force
