vlib work
vlib msim

vlib msim/xil_defaultlib
vlib msim/xpm

vmap xil_defaultlib msim/xil_defaultlib
vmap xpm msim/xpm

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../../rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/hdl/verilog" "+incdir+../../../../../rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/hdl/verilog" \
"C:/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2016.4/data/ip/xpm/xpm_VCOMP.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../../rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/hdl/verilog" "+incdir+../../../../../rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/hdl/verilog" \
"../../../../../rgmii_mac/rgmii_mac.srcs/sources_1/ip/ila_2/sim/ila_2.v" \

vlog -work xil_defaultlib "glbl.v"

