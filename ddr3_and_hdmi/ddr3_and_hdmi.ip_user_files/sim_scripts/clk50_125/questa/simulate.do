onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib clk50_125_opt

do {wave.do}

view wave
view structure
view signals

do {clk50_125.udo}

run -all

quit -force
