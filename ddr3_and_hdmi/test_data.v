module test_data(
input 	wire 		sclk		,
input 	wire 		rst_n		,
output 	reg[27:0]	wr_cmd_addr_in	,
output 	reg		wr_en_in	,
output 	reg[127:0]	wr_data_in	,
output 	reg		data_en		
);

reg 		flag;
reg[10:0]	cnt_data;

//wr_cmd_addr_in
always @ (posedge sclk)
	if(rst_n == 0)
		wr_cmd_addr_in	<=	0;
	else if((flag==1)&&(wr_en_in==1))
		wr_cmd_addr_in  <=	wr_cmd_addr_in+512;
		
//wr_data_in
always @ (posedge sclk)
	if(rst_n == 0)
		wr_data_in	<=	0;
	else if(data_en==1&&flag==1)
		wr_data_in	<=	wr_data_in+128'h01010101_01010101_01010101_01010101;
		//wr_data_in	<=	wr_data_in+128'h00000000_00000000_00000000_00000001;

//flag
always @ (posedge sclk)
	if(rst_n == 0)
		flag<=0;
	else if(wr_cmd_addr_in==3072*512)
		flag<=0;
	else
		flag<=1;
	
//data_en
always @ (posedge sclk)
	if(rst_n == 0)
		data_en<=0;
	else if(cnt_data<64&&flag==1)
		data_en<=1;
	else 
		data_en<=0;
		
//wr_en_in
always @ (posedge sclk)
	if(rst_n == 0)
		wr_en_in<=0;
	else if((cnt_data==400)&&(flag==1))
		wr_en_in<=1;
	else
		wr_en_in<=0;
		
//cnt_data
always @ (posedge sclk)
	if(rst_n == 0)
		cnt_data<=0;
	else if(cnt_data==800&&flag==1)
		cnt_data<=0;
	else if(flag==1)
		cnt_data<=cnt_data+1;
		

endmodule
