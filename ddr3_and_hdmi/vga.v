module vga(
input 	wire 		sclk		,
input 	wire 		rst_n		,
input 	wire[23:0] 	data		,
output 	wire		de		,
output 	wire		po_start_flag	,
output 	wire[7:0] 	r		,
output 	wire[7:0] 	g		,
output 	wire[7:0]	b		,
output  wire		h_sync		,
output 	wire		v_sync		
);

parameter 	H_SYNC 		=	135,//h_sync结束的时间		1,//
		H_BACK_PROCH 	=	295,//H_BACK_PROCH结束的时间	2,//
		H_ADDR_TIME	=	1319,//H_ADDR_TIME结束的时间    3,//
		H_FRONT_PROCH 	=	1343,//H_FRONT_PROCH结束的时间  4,//
		V_SYNC 		=	5,//v_sync结束的时间            1,//
		V_BACK_PROCH 	=	34,//V_BACK_PROCH结束的时间     2,//
		V_ADDR_TIME 	=	802,//V_ADDR_TIME结束的时间     3,//
		V_FRONT_PROCH 	= 	805;//V_FRONT_PROCH结束的时间   4;//
		

reg[10:0]	h_cnt	;
reg[9:0]	v_cnt	;


assign 	de	=((h_cnt>H_BACK_PROCH)&&(h_cnt<=H_ADDR_TIME))////是否在有效区域内
		 &&((v_cnt>V_BACK_PROCH)&&(v_cnt<=V_ADDR_TIME))?1'b1:1'b0;
		 
assign 	{r,g,b}	=((h_cnt>H_BACK_PROCH)&&(h_cnt<=H_ADDR_TIME))
		 &&((v_cnt>V_BACK_PROCH)&&(v_cnt<=V_ADDR_TIME))?data:24'h000000;
		 
assign	po_start_flag = (v_cnt>V_ADDR_TIME)?1'b1:1'b0;
assign	h_sync = (h_cnt<=H_SYNC)?1'b1:1'b0;
assign	v_sync = (v_cnt<=V_SYNC)?1'b1:1'b0;


/*
//v_sync
always @ (posedge sclk)
	if(rst_n == 0 )
		v_sync<=1'b0;
	else if(v_cnt <= V_SYNC)
		v_sync<=1'b1;
	else
		v_sync<=1'b0;

//h_sync
always @ (posedge sclk)
	if(rst_n == 0 )
		h_sync<=1'b0;
	else if(h_cnt <= H_SYNC)
		h_sync<=1'b1;
	else
		h_sync<=1'b0;
*/
//h_cnt
always @ (posedge sclk)
	if(rst_n == 0 )
		h_cnt <= 11'b0;
	else if(h_cnt == H_FRONT_PROCH )
		h_cnt <= 11'b0;
	else 
		h_cnt <= h_cnt + 11'b1;
		
		
//v_cnt
always @ (posedge sclk)
	if(rst_n == 0 )
		v_cnt <= 10'b0;
	else if((h_cnt == H_FRONT_PROCH )&&(v_cnt == V_FRONT_PROCH))
		v_cnt <= 10'b0;
	else if(h_cnt == H_FRONT_PROCH )
		v_cnt <= v_cnt + 10'b1;
endmodule
