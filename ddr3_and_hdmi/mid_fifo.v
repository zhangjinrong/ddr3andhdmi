
module mid_fifo(
input 	wire 		clk_200		,
input 	wire 		clk_65		,
input 	wire 		rst_n		,
input 	wire[127:0]	data_in		,
input 	wire 		have_data	,
input 	wire 		de		,
input 	wire 		po_flag_start	,
output 	wire[23:0]	data_out	,
output 	reg		ddr3_req	,
output	reg[27:0]	cmd_addr	,
output 	reg		cmd_en2		,
input 	wire[6:0]	rd_data_count1	
);


parameter	IDLE 	= 	1,
		WAIT	= 	2,
		REQ 	= 	3,
		RD 	= 	4;
		
		
wire[23:0]		dout1;
wire[31:0] 		dout;
wire[12:0]		rd_data_count;
reg 			can_read;
reg			have_data1;
reg 			have_data2;
reg[7:0]		data_cnt;
reg 			read_end;
reg[3:0]		state;
wire 			rd_en;

reg 			deal_have_data;
reg 			read_begin_flag;
reg			rst_n1;
reg[7:0]		cnt_rst;

wire			ddr3_req_fifo;
reg 			cmd_en1;
reg 			cmd_en;



assign  ddr3_req_fifo = (rst_n&&rst_n1)?ddr3_req:0;
assign  rd_en = de&&can_read;
assign 	dout1 = dout[23:0];
assign 	data_out = (de&&can_read)?dout1:24'h000000;


//cmd_en2
always @ (posedge clk_200)
	if(rst_n == 0)
		cmd_en2<=0;
	else if(cmd_en==1&&cmd_en1==0)
		cmd_en2<=1;
	else
		cmd_en2<=0;
		
//cmd_en1
always @ (posedge clk_200)
	if(rst_n == 0)
		cmd_en1<=0;
	else
		cmd_en1<=cmd_en;

//rst_n1
always @ (posedge clk_200)
	if(rst_n == 0)
		rst_n1<=0;
	else if(cnt_rst<20)
		rst_n1<=0;
	else if(cnt_rst==20)
		rst_n1<=1;

//cnt_rst
always @ (posedge clk_200)
	if(rst_n == 0)
		cnt_rst<=0;
	else if(cnt_rst==20)
		cnt_rst<=20;
	else
		cnt_rst<=cnt_rst+1;
		
//read_begin_flag
always @ (posedge clk_200)
	if(rst_n1 == 0)
		read_begin_flag<=0;
	else if((deal_have_data==0)&&(have_data==1))
		read_begin_flag<=1;
	else
		read_begin_flag<=0;
		
//deal_have_data
always @ (posedge clk_200)
	if(rst_n1 == 0)
		deal_have_data<=0;
	else if(have_data==0)
		deal_have_data<=0;
	else if(read_begin_flag==1)
		deal_have_data<=1;



//cmd_addr
always @ (posedge clk_200)
	if(rst_n1 == 0)
		cmd_addr<=0;
	else if((cmd_en2==1)&&(cmd_addr==3071*512))
		cmd_addr<=0;
	else if(cmd_en2==1)
		cmd_addr<=cmd_addr+512;
		
//cmd_en
always @ (posedge clk_200)
	if(rst_n1 == 0)
		cmd_en<=0;
	else if(state == REQ)
		cmd_en<=1;
	else
		cmd_en<=0;
//state
always @ (posedge clk_200)
	if(rst_n1 == 0)
		state<=IDLE;
	else case(state)
		IDLE:
			state<=WAIT;
		WAIT:
			if(rd_data_count<2400)
				state<=REQ;
			else if(rd_data_count>=2400)
				state<=WAIT;
		REQ:
			if(cmd_en)
				state<=RD;
		RD:
			if(read_end==1)
				state<=WAIT;
		default:
			state<=IDLE;
		endcase
		
//read_end
always @ (posedge clk_200)
	if(rst_n1 == 0)
		read_end<=0;
	else if(data_cnt==63)
		read_end<=1;
	else
		read_end<=0;
		
//data_cnt
always @ (posedge clk_200)
	if(rst_n1 == 0)
		data_cnt<=0;
	else if(ddr3_req==1)
		data_cnt<=data_cnt+1;
	else if(ddr3_req==0)
		data_cnt<=0;
		
		
//have_data1
always @ (posedge clk_200)
	if(rst_n1 == 0)
		have_data1<=0;
	else 
		have_data1<=have_data;
		
//have_data2
always @ (posedge clk_200)
	if(rst_n1 == 0)
		have_data2<=0;
	else if((have_data==1)&&(have_data1==0))
		have_data2<=1;
	else 
		have_data2<=0;
		
//ddr3_req
always @ (posedge clk_200)
	if(rst_n == 0)
		ddr3_req <= 1;
	//else if(read_begin_flag==1)	
	//else if(have_data2==1)
	else if((rst_n==1)&&(rst_n1==0))
		ddr3_req<=0;
	else if(rd_data_count1>=50)
		ddr3_req<=1;
	else if(data_cnt==63)
		ddr3_req<=0;
/*
//2		
ila_1 ila_1_inst1 (
.clk(clk_200), // input wire clk
.probe0({4'b0,rd_data_count,data_out,can_read,de,rd_data_count1,rd_en,dout[23:0],data_in[23:0],ddr3_req_fifo}) // input wire [99:0] probe0
);
*/

//can_read
always @ (posedge clk_65)
	if(rst_n1 == 0)
		can_read <= 0;
	else if(po_flag_start&&(rd_data_count>2400))
		can_read<=1;


fifo_midfifo fifo_midfifo_inst (
.wr_clk(clk_200),                // input wire wr_clk
.rd_clk(clk_65),                // input wire rd_clk
.din(data_in),                      // input wire [127 : 0] din
.wr_en(ddr3_req_fifo),                  // input wire wr_en
.rd_en(rd_en),                  // input wire rd_en
.dout(dout),                    // output wire [31 : 0] dout
.full(),                    // output wire full
.empty(),                  // output wire empty
.rd_data_count(rd_data_count),  // output wire [12 : 0] rd_data_count
.wr_data_count()  // output wire [10 : 0] wr_data_count
);
endmodule
