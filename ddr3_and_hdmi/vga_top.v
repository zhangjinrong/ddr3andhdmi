

module vga_top(
input 	wire 		clk_65		,
input		wire 		clk_325		,
input 	wire 		rst_n		,
input 	wire[23:0] 	data		,
output 	wire		de		,
output 	wire		po_start_flag	,

output 	wire 		r_ser_data_p	,
output 	wire 		r_ser_data_n	,
output 	wire 		g_ser_data_p	,
output 	wire 		g_ser_data_n	,
output 	wire 		b_ser_data_p	,
output 	wire 		b_ser_data_n	,
output 	wire 		c_ser_data_p	,
output 	wire 		c_ser_data_n	,
output  wire 		hdmi_en			
//output 	wire		clk_65				
);

wire[9:0] 	r1		;
wire[9:0] 	g1		;
wire[9:0]	b1		;
wire[7:0] 	r		;
wire[7:0] 	g		;
wire[7:0]	b		;
wire		h_sync		;
wire		v_sync		;
//wire 		clk_65		;
//wire 		clk_325		;
//wire		de		;

assign 	hdmi_en = 1;
/*
vga_pic vga_pic_inst(

.clk     	(sclk		), 
.rst_n    	(rst_n		),
.pi_data  	(24'hff0000	),

.hsync    	(h_sync		),
.vsync    	(v_sync		),
.r        	(r		),
.g       		(g		),
.b        	(b		),
.data_en    (de		)
);
*/
vga vga_inst(
.sclk							(clk_65		),
.rst_n						(rst_n		),
.data							(data),
.de								(de		),
.po_start_flag		(po_start_flag	),
.r								(r		),
.g								(g		),
.b								(b		),
.h_sync						(h_sync		),
.v_sync						(v_sync		)
);

par2ser r_par2ser(
.clk_1x			(clk_65		),
.clk_5x			(clk_325	),
.rst_n			(rst_n		),
.par_data		(r1		),
.ser_data_p		(r_ser_data_p	),
.ser_data_n		(r_ser_data_n	)
);

par2ser g_par2ser(
.clk_1x			(clk_65		),
.clk_5x			(clk_325	),
.rst_n			(rst_n		),
.par_data		(g1		),
.ser_data_p		(g_ser_data_p	),
.ser_data_n		(g_ser_data_n	)
);

par2ser b_par2ser(
.clk_1x			(clk_65		),
.clk_5x			(clk_325	),
.rst_n			(rst_n		),
.par_data		(b1		),
.ser_data_p		(b_ser_data_p	),
.ser_data_n		(b_ser_data_n	)
);

par2ser c_par2ser(
.clk_1x			(clk_65		),
.clk_5x			(clk_325	),
.rst_n			(rst_n		),
.par_data		(10'b11111_00000),
.ser_data_p		(c_ser_data_p	),
.ser_data_n		(c_ser_data_n	)
);


encode  r_encode(
.clkin		(clk_65),    // pixel clock input
.rstin		(~rst_n),    // async. reset input (active high)
.din		(r),      // data inputs: expect registered
.c0		(0),       // c0 input
.c1		(0),       // c1 input
.de		(de),       // de input
.dout		(r1)      // data outputs
);

encode  g_encode(
.clkin		(clk_65),    // pixel clock input
.rstin		(~rst_n),    // async. reset input (active high)
.din		(g),      // data inputs: expect registered
.c0		(0),       // c0 input
.c1		(0),       // c1 input
.de		(de),       // de input
.dout		(g1)      // data outputs
);

encode  b_encode(
.clkin		(clk_65),    // pixel clock input
.rstin		(~rst_n),    // async. reset input (active high)
.din		(b),      // data inputs: expect registered
.c0		(h_sync),       // c0 input
.c1		(v_sync),       // c1 input
.de		(de),       // de input
.dout		(b1)      // data outputs
);

/*
clk_65_325 clk_65_325_inst
(
// Clock out ports
.clk_65M(clk_65),     // output clk_65M
.clk_325M(clk_325),     // output clk_325M
// Clock in ports
.clk_50M(sclk));      // input clk_50M
*/

endmodule
