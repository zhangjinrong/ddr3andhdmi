

module test_mid_fifo(
input 	wire 	sclk,
input 	wire 	rst_n,
input 	wire	cmd_en,

output 	reg[6:0]	have_data
);


//have_data
always @ (posedge sclk)
	if(rst_n == 0)
		have_data<=0;
	else if(cmd_en==1)
		have_data<=60;
	else
		have_data<=0;
		
endmodule
