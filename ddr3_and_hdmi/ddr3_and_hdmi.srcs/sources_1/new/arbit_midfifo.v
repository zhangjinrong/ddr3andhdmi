module arbit_midfifo(
input	wire		clk		,
input	wire		rst_n		,
input	wire[23:0]	wr_ddr3_addr	,
input	wire		tx_end_read	,

output	reg		tx_en 		,
output	reg		hdmi_en	,//只是给hdmi一个工作使能
output	reg		tx_can_read	
);


//tx_en
always @ (posedge clk)
	if(rst_n == 0)
		tx_en	<=	1;
	else if(tx_end_read)
		tx_en	<=	0;

//hdmi_en
always @ (posedge clk)
	if(rst_n == 0)
		hdmi_en	<=	0;
	else if(tx_end_read)
		hdmi_en	<=	1;

//tx_can_read
always @ (posedge clk)
	if(rst_n == 0)
		tx_can_read	<=	0;
	else if(wr_ddr3_addr==3070*512)
		tx_can_read	<=	1;
	else if(wr_ddr3_addr==3071*512)
		tx_can_read	<=	0;


endmodule
