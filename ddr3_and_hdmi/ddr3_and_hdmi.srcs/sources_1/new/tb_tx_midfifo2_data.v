

module tb_tx_midfifo2_data(
input	wire		clk_125		,
input	wire		rst_n			,
input	wire		ddr3_cmd		,
input	wire		ddr3_data_en		,
output	reg[127:0]	ddr3_data		,
output	reg[6:0]	ddr3_data_count	,

output	reg		mac_en			
    );

reg[4:0]	cnt	;


//mac_en
always @ (posedge clk_125)
	if(rst_n == 0)
		mac_en	<=	0;
	else if(cnt<20)
		mac_en	<=	1;
	else 
		mac_en	<=	0;
		
//cnt
always @ (posedge clk_125)
	if(rst_n == 0)
		cnt	<=	0;
	else 
		cnt	<=	cnt+1; 

//ddr3_data_count
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_data_count	<=	0;
	else if(ddr3_cmd)
		ddr3_data_count	<=	60;
	else 
		ddr3_data_count	<=	0;
		
//ddr3_data
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_data	<=	128'h0;
	else if(ddr3_data_en)
		ddr3_data	<=	ddr3_data+1;
		
		

endmodule
