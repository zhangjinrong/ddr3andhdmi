module tx_mid_fifo2(
input	wire		clk_125		,
input	wire		rst_n			,
input	wire		tx_can_read		,
input	wire[6:0]	ddr3_data_count	,
input	wire[127:0]	ddr3_data		,
output	reg		ddr3_data_en		,
output	reg		ddr3_cmd		,
output	reg[27:0]	ddr3_cmd_addr		,

input	wire		mac_en			,
input	wire		tx_end_read		,
output	reg		mac_can_read		,
output	wire[31:0]	mac_data		
);

parameter	IDLE	=	0	,
		READ	=	1	,
		WAIT	=	2	;
		
parameter	YUZHI	=	2000	;

reg		midfifo_start		;
reg[3:0]	state			;
reg		ddr3_cmd1		;
reg		ddr3_cmd2		;
wire[10:0]	tx_data_count		;
reg[10:0]	ddr3_data_cnt		;
reg		mac_can_read1		;
reg		mac_can_read2		;

//mac_can_read
always @ (posedge clk_125)
	if(rst_n == 0)
		mac_can_read	<=	0;
	else if(mac_can_read1&&(!mac_can_read2))
		mac_can_read	<=	1;
	else
		mac_can_read	<=	0;
		
//mac_can_read2
always @ (posedge clk_125)
	if(rst_n == 0)
		mac_can_read2	<=	0;
	else 
		mac_can_read2	<=	mac_can_read1;

//mac_can_read1
always @ (posedge clk_125)
	if(rst_n == 0)
		mac_can_read1	<=	0;
	else if(!midfifo_start)
		mac_can_read1	<=	0;
	else if(tx_data_count>=YUZHI)
		mac_can_read1	<=	1;
		
//ddr3_data_cnt
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_data_cnt	<=	0;
	else if(ddr3_data_en)
		ddr3_data_cnt	<=	ddr3_data_cnt+1;
	else if(!ddr3_data_en)
		ddr3_data_cnt	<=	0;

//ddr3_data_en
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_data_en	<=	0;
	else if((state==READ)&&(ddr3_data_count==40))
		ddr3_data_en	<=	1;
	else if(ddr3_data_cnt==63)
		ddr3_data_en	<=	0;

//ddr3_cmd_addr
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_cmd_addr	<=	0;
	else if((ddr3_cmd_addr==3071*512)&&(ddr3_cmd))
		ddr3_cmd_addr	<=	0;
	else if(ddr3_cmd)
		ddr3_cmd_addr	<=ddr3_cmd_addr+512;
	
//ddr3_cmd
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_cmd	<=	0;
	else if(ddr3_cmd1&&(!ddr3_cmd2))
		ddr3_cmd	<=	1;
	else
		ddr3_cmd	<=	0;

//ddr3_cmd2
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_cmd2	<=	0;
	else 
		ddr3_cmd2	<=	ddr3_cmd1;

//ddr3_cmd1
always @ (posedge clk_125)
	if(rst_n == 0)
		ddr3_cmd1	<=	0;
	else if(ddr3_data_cnt==63)
		ddr3_cmd1	<=	0;
	else if((state==READ)&&(tx_data_count<YUZHI))
		ddr3_cmd1	<=	1;
	
//state
always @ (posedge clk_125)
	if(rst_n == 0)
		state	<=	IDLE	;
	else if(midfifo_start&&(tx_data_count<YUZHI))
		state	<=	READ	;
	else if(midfifo_start&&(tx_data_count>=YUZHI))
		state	<=	WAIT	;
	else if(!midfifo_start)
		state	<=	IDLE	;

//midfifo_start
always @ (posedge clk_125)
	if(rst_n == 0)
		midfifo_start	<=	0;
	else if(tx_can_read)
		midfifo_start	<=	1;
	else if(tx_end_read)
		midfifo_start	<=	0;

fifo_tx_mid2 fifo_tx_mid2_inst (
  .clk(clk_125),                      // input wire clk
  .din(ddr3_data),                      // input wire [127 : 0] din
  .wr_en(ddr3_data_en),                  // input wire wr_en
  .rd_en(mac_en),                  // input wire rd_en
  .dout(mac_data),                    // output wire [31 : 0] dout
  .full(),                    // output wire full
  .empty(),                  // output wire empty
  .rd_data_count(tx_data_count),  // output wire [12 : 0] rd_data_count
  .wr_data_count()  // output wire [10 : 0] wr_data_count
);
endmodule
