// Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
// Date        : Tue Oct 10 21:23:34 2017
// Host        : DESKTOP-K9RC7KV running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_stub.v
// Design      : clk_65_235
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tfgg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module clk_65_235(clk_65, clk_325, clk_50)
/* synthesis syn_black_box black_box_pad_pin="clk_65,clk_325,clk_50" */;
  output clk_65;
  output clk_325;
  input clk_50;
endmodule
