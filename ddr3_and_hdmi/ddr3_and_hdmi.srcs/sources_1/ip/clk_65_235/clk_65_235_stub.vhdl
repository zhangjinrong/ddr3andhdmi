-- Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2016.4 (win64) Build 1756540 Mon Jan 23 19:11:23 MST 2017
-- Date        : Tue Oct 10 21:23:34 2017
-- Host        : DESKTOP-K9RC7KV running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/Users/r/Desktop/fpga_autumn/ddr3_and_hdmi/ddr3_and_hdmi.srcs/sources_1/ip/clk_65_235/clk_65_235_stub.vhdl
-- Design      : clk_65_235
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tfgg484-2
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity clk_65_235 is
  Port ( 
    clk_65 : out STD_LOGIC;
    clk_325 : out STD_LOGIC;
    clk_50 : in STD_LOGIC
  );

end clk_65_235;

architecture stub of clk_65_235 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_65,clk_325,clk_50";
begin
end;
