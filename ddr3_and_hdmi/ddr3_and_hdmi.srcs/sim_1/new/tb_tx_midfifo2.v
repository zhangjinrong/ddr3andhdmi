`timescale 1ns / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/10/15 17:36:40
// Design Name: 
// Module Name: tb_tx_midfifo2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_tx_midfifo2();

reg		clk_125		;
reg		rst_n			;
reg		tx_can_read		;
wire		ddr3_cmd		;
wire		ddr3_data_en		;
wire[127:0]	ddr3_data		;
wire[6:0]	ddr3_data_count	;
wire		mac_en			;

initial
	begin
		clk_125	=	0;
		rst_n		<=	0;
		tx_can_read	<=	0;
		#100
		rst_n		<=	1;
		#100
		tx_can_read	<=	1;
		#20
		tx_can_read	<=	0;
	end

always #10	clk_125	=	~clk_125	;


tb_tx_midfifo2_data inst_tb_tx_midfifo2_data(
.clk_125		(clk_125		       ),
.rst_n			(rst_n			       ),
.ddr3_cmd		(ddr3_cmd		       ),
.ddr3_data_en		(ddr3_data_en		       ),
.ddr3_data		(ddr3_data		       ),
.ddr3_data_count	(ddr3_data_count	       ),

.mac_en		(mac_en			)
    );

tx_mid_fifo2 tx_mid_fifo2_inst(
.clk_125		(clk_125),
.rst_n			(rst_n),
.tx_can_read		(tx_can_read),
.ddr3_data_count	(ddr3_data_count),
.ddr3_data		(ddr3_data),
.ddr3_data_en		(ddr3_data_en),
.ddr3_cmd		(ddr3_cmd),
.ddr3_cmd_addr	(),
                     
.mac_en		(mac_en),
.tx_end_read		(),
.mac_can_read		(),
.mac_data		()
);



endmodule
