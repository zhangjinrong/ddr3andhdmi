`timescale 1ns / 1ns
module tb_uart();
reg		sclk	;
reg		rst_n	;
reg		rx		;
wire	tx		;
reg[7:0]	get_data;

initial
	begin
			sclk	=		0;
			rst_n	<=	0;
			get_data<=8'b0;
			rx<=1;
			#100
			rst_n	<=	1;			
	end

initial	begin
	data();
	end

always #10 sclk = ~sclk	;

task	data();
	integer		i;
	@(posedge	rst_n)
	for(i=0;i<100;i=i+1)
		begin				
				get_data<=get_data+8'b0101_0101;
				rx<=0;
				#8680
				for(i=0;i<8;i=i+1)
					begin
						#8680
						rx<=get_data[i];
					end
				#8680
					rx<=1;
		end
endtask		

/*
task	rx_data();
integer	i;
@(posedge	rst_n)
	rx<=0;
	for(i=0;i<8;i=i+1)
		begin
				#434
				rx<=get_data[i];
		end
	#434;
	rx<=1;
	#434;
endtask
*/

top_user_wr_ctrl top_user_wr_ctrl_inst(
.sclk		(sclk		),
.rst_n	(rst_n	),
.rx			(rx			),
.tx			(tx			)
);

endmodule
