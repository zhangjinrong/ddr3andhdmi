`timescale 1ns / 1ns


module tb_ddr3_rgmii();

reg		   sclk		;
reg		   rst_n		;
wire[15:0]       ddr3_dq		;
wire[1:0]        ddr3_dqs_n		;
wire[1:0]        ddr3_dqs_p		;
wire[13:0]       ddr3_addr		;
wire[2:0]        ddr3_ba		;
wire             ddr3_ras_n		;
wire             ddr3_cas_n		;
wire             ddr3_we_n		;
wire             ddr3_reset_n	;
wire[0:0]        ddr3_ck_p		;
wire[0:0]        ddr3_ck_n		;
wire[0:0]        ddr3_cke		;
wire[0:0]        ddr3_cs_n		;
wire[1:0]        ddr3_dm		;
wire[0:0]        ddr3_odt		;

initial
		begin
				sclk=0;
				rst_n<=0;
				#100
				rst_n<=1;
		end

always #10 sclk<=	~sclk	;

top_all top_all_inst(
.sclk					(sclk					),
.rst_n				(rst_n				),
/*
.rx_clk			,
.rx_data		,
.rx_ctrl		,
.phy_rst_n	
*/
//----------------------ddr3
.ddr3_dq			(ddr3_dq			),
.ddr3_dqs_n		(ddr3_dqs_n		),
.ddr3_dqs_p		(ddr3_dqs_p		),
.ddr3_addr		(ddr3_addr		),
.ddr3_ba			(ddr3_ba			),
.ddr3_ras_n		(ddr3_ras_n		),
.ddr3_cas_n		(ddr3_cas_n		),
.ddr3_we_n		(ddr3_we_n		),
.ddr3_reset_n	(ddr3_reset_n	),
.ddr3_ck_p		(ddr3_ck_p		),
.ddr3_ck_n		(ddr3_ck_n		),
.ddr3_cke			(ddr3_cke			),
.ddr3_cs_n		(ddr3_cs_n		),
.ddr3_dm			(ddr3_dm			),
.ddr3_odt			(ddr3_odt			)
//----------------------hdmi
/*
output 	wire 		r_ser_data_p	,
output 	wire 		r_ser_data_n	,
output 	wire 		g_ser_data_p	,
output 	wire 		g_ser_data_n	,
output 	wire 		b_ser_data_p	,
output 	wire 		b_ser_data_n	,
output 	wire 		c_ser_data_p	,
output 	wire 		c_ser_data_n	,
output  wire 		hdmi_en				
*/
);
endmodule
