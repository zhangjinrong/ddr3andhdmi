
module arbit(
input 	wire 		sclk			,
input 	wire 		rst_n			,
//----------------------------------------write----------------------------
output	reg		wr_en			,//
output wire          app_wdf_rdy_out	,//
input 	wire 		wr_req			,//(empty)
output wire 		wr_app_rdy		,//
input 	wire[27:0]    wr_app_addr		,//
input 	wire[2:0]     wr_app_cmd		,//
input  wire          wr_app_en		,//
input  wire[127:0]   app_wdf_data_in	,//
input  wire          app_wdf_end_in	,//
input 	wire[15:0]    app_wdf_mask_in	,//
input  wire          app_wdf_wren_in	,//
//---------------------------------------read--------------------------------
output reg 		rd_en			,//
output wire 		rd_app_rdy		,//
input 	wire 		rd_req			,//(empty)
output wire[127:0]   app_rd_data_out	,//
output wire          app_rd_data_valid_out,//
input  wire[2:0]     rd_app_cmd		,//
input  wire        	rd_app_en		,//
input 	wire[27:0]	rd_app_addr		,//
//---------------------------------------right-----------------------------
input  wire		app_rd_data_valid	,//
input  wire[127:0]	app_rd_data		,//
input  wire 		app_rdy		,//
input  wire 		app_wdf_rdy		,//
output wire[127:0] 	app_wdf_data		,//
output wire 		app_wdf_end		,//
output wire 		app_wdf_wren		,//
output wire[15:0]	app_wdf_mask		,//
output reg[27:0]     app_addr		,//
output reg[2:0]      app_cmd		,//
output reg       	app_en			 	
);

assign app_wdf_end			=	app_wdf_end_in	;
assign app_wdf_mask			=	app_wdf_mask_in	;
assign app_wdf_wren			=	app_wdf_wren_in	;
assign app_wdf_data			=	app_wdf_data_in	;
assign app_wdf_rdy_out 		= 	app_wdf_rdy		;
assign wr_app_rdy			=	app_rdy		;
assign rd_app_rdy			=	app_rdy		;
assign app_rd_data_out 		= 	app_rd_data		;
assign app_rd_data_valid_out	=	app_rd_data_valid	;


parameter	IDLE	=	0,
		WAIT 	= 	1,
		WR 	=	2,
		RD 	=	3;

reg [2:0]	state		;
reg [13:0]	robin		;
reg 		robin_flag	;
reg 		wr_end		;
reg 		wr_app_en1	;
reg		rd_end		;
reg 		rd_app_en1	;
reg 		wr_req1	;
reg 		rd_req1	;

//wr_req1
always @ (posedge sclk)
	if(rst_n == 0 )
		wr_req1<=0;
	else if(state==WR)
		wr_req1<=0;
	else if(wr_req==1)
		wr_req1<=1;
	
		
//rd_req1
always @ (posedge sclk)
	if(rst_n == 0 )
		rd_req1<=0;
	else if(state==RD)
		rd_req1<=0;
	else if(rd_req==1)
		rd_req1<=1;
	
		
//rd_app_en1
always @ (posedge sclk)
	if(rst_n==0)
		rd_app_en1<=0;
	else
		rd_app_en1<=rd_app_en;

//rd_end
always @ (posedge sclk)
	if(rst_n == 0)
		rd_end<=0;
	else if((rd_app_en==0)&&(rd_app_en1==1))
		rd_end<=1;
	else 
		rd_end<=0;

//wr_app_en1
always @ (posedge sclk)
	if(rst_n==0)
		wr_app_en1<=0;
	else
		wr_app_en1<=wr_app_en;

//wr_end
always @ (posedge sclk)
	if(rst_n == 0)
		wr_end<=0;
	else if((wr_app_en==0)&&(wr_app_en1==1))
		wr_end<=1;
	else 
		wr_end<=0;



//app_addr,app_cmd,app_en	这里注意，线性赋值要时刻给条件！！！
always @ *
	if(rst_n == 0)
		begin
			app_addr	=		0;
			app_cmd	=      	0;
			app_en		=      	0;
		end	
		
	else	if(wr_app_en)//if(wr_req1&&(state==WR))
		begin
			app_addr	=	wr_app_addr	;
			app_cmd	=      wr_app_cmd	;
			app_en		=      wr_app_en	;
		end	
	else	if(rd_app_en)//if(rd_req1&&(state==RD))
		begin
			app_addr	=	rd_app_addr	;
			app_cmd	=      rd_app_cmd	;
			app_en		=      rd_app_en	;
		end	
	else
		begin
			app_addr	=	wr_app_addr	;
			app_cmd	=       wr_app_cmd	;
			app_en		=       wr_app_en	;
		end	
	

//robin_flag
always @ (posedge sclk)
	if(rst_n==0)
		robin_flag<=0;
	else if(robin<50)
		robin_flag<=1;
	else
		robin_flag<=0;

//robin
always @ (posedge sclk)
	if(rst_n==0)
		robin<=0;
	else if(robin==99)
		robin<=0;
	else 
		robin<=robin+1;

//state
always @ (posedge sclk)
	if(rst_n == 0)
		state <= IDLE;
	else case(state)
	IDLE:
		state<=WAIT;
	WAIT:
		if(wr_req1)
				state<=WR;
			else if(rd_req1)
				state<=RD;
		/*if(robin_flag==1)
			if(wr_req1)
				state<=WR;
			else if(rd_req1)
				state<=RD;
		else if(robin_flag==0)
			if(rd_req1)
				state<=RD;
			else if(wr_req1)
				state<=WR;*/
	WR:
		if(wr_end==1)
			state<=WAIT;
	RD:
		if(rd_end==1)
			state<=WAIT;
	default:
		state<=WAIT;
	endcase

//wr_en
always @ (posedge sclk)
	if(rst_n == 0)
		wr_en<=0;
	else if(wr_req1&&(state==WR))	
		wr_en<=1;
	else 
		wr_en<=0;	

//rd_en
always @ (posedge sclk)
	if(rst_n == 0)
		rd_en<=0;
	else if(rd_req1&&(state==RD))	
		rd_en<=1;
	else 
		rd_en<=0;	

endmodule
