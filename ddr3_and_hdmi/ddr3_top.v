
module ddr3_top(
input 		wire 		rst_n		,
input		wire		clk_125	,
input		wire		clk_200M,
input   	wire 		data_en_out	,//读数据使能
output  	wire[127:0]	rd_data_out	,//读出的数据
//output 	wire		have_data	,//有没有数据，为0则有
input 		wire[27:0]	rd_cmd_addr_in,//读命令地址进入
input   	wire 		rd_en_in	,//读命令使能
output 	wire[6:0] 	rd_data_count	,

input 		wire[27:0]	wr_cmd_addr_in	,
input 		wire 		wr_en_in	,
input 		wire[127:0]	wr_data_in	,
input 		wire 		data_en_in	,

inout 		wire[15:0]       ddr3_dq	,
inout 		wire[1:0]        ddr3_dqs_n	,
inout 		wire[1:0]        ddr3_dqs_p	,
output 	wire[13:0]       ddr3_addr	,
output 	wire[2:0]        ddr3_ba	,
output  	wire             ddr3_ras_n	,
output  	wire             ddr3_cas_n	,
output  	wire             ddr3_we_n	,
output  	wire             ddr3_reset_n	,
output 	wire[0:0]        ddr3_ck_p	,
output 	wire[0:0]        ddr3_ck_n	,
output 	wire[0:0]        ddr3_cke	,
output 	wire[0:0]        ddr3_cs_n	,
output 	wire[1:0]        ddr3_dm	,
output 	wire[0:0]        ddr3_odt	,
output  	wire 		   init_calib_complete	,
output 	wire 		   ui_clk	
);

wire[27:0]	wr_cmd_addr_in1	;
wire 		wr_en_in1	;
wire[127:0]	wr_data_in1	;
wire 		data_en_in1	;


//--------------------------------用户端
wire[27:0]	app_addr		;
wire[2:0]	app_cmd			;
wire		app_en			;
wire[127:0]	app_wdf_data		;
wire		app_wdf_end		;
wire		app_wdf_wren		;
wire[15:0]	app_wdf_mask		;
wire 		app_rd_data_valid	;
wire[127:0]	app_rd_data		;
wire 		app_rdy			;
wire 		app_wdf_rdy		;
//----------------------------------读写 fifo  
wire		wr_cmd_req		;
wire 		wr_cmd_en		;
wire[27:0] 	wr_cmd_addr		;
wire[127:0]	data_in			;
wire 		data_req		; 
wire 		rd_cmd_req		;
wire		rd_cmd_en		;
wire[27:0] 	rd_cmd_addr		;
wire[127:0]	app_rd_data_out		;
wire 		app_rd_data_valid_out	;


//wire 		data_en_out	;
//wire[127:0]	rd_data_out	;
//wire		have_data	;
//wire[27:0]	rd_cmd_addr_in	;
//wire 		rd_en_in	;
//wire[27:0]	wr_cmd_addr_in		;
//wire 		wr_en_in		;
//wire[127:0]	wr_data_in		;
//wire 		data_en_in		;


//assign 		wr_cmd_addr_in	=  0    ;
//assign		wr_en_in	=  0	;
//assign 		data_en_in	=  0    ;
//assign		wr_data_in	=  0	;
/*
ila_0 ila_0_inst1 (
.clk(ui_clk), // input wire clk
.probe0({25'b0,init_calib_complete,rd_cmd_addr_in[23:0],rd_en_in,rd_data_out[23:0],app_rd_data_valid_out,app_rd_data_out[23:0]}) // input wire [99:0] probe0
);

//1

ila_1 your_instance_name2(
.clk(ui_clk), // input wire clk
.probe0({app_rd_data_valid_out,app_rd_data_out[23:0],init_calib_complete,wr_cmd_addr_in[22:0],wr_en_in,data_in[23:0],rd_en_in,wr_data_in[22:0],data_req,data_en_in}) // input wire [99:0] probe0
);
*/
/*
gen_data gen_data_inst(
.sclk		(ui_clk		),
.rst_n		(init_calib_complete),
.wr_cmd_addr_in	(wr_cmd_addr_in	),//
.wr_en_in	(wr_en_in	),//
.rd_cmd_addr_in	(rd_cmd_addr_in	),//
.rd_en_in	(rd_en_in	),//
.wr_data_in	(wr_data_in	),//
.data_en_in	(data_en_in	)//
);
*/
/*
test_data test_data_inst(
.sclk		(clk_125			),
.rst_n		(init_calib_complete	),
.wr_cmd_addr_in	(wr_cmd_addr_in1		),
.wr_en_in	(wr_en_in1		),
.wr_data_in	(wr_data_in1		),
.data_en	(data_en_in1		)
);
*/


cmd_fifo wr_cmd_fifo (
.wr_clk(clk_125),  // input wire wr_clk
.rd_clk(ui_clk),  // input wire rd_clk
.din(wr_cmd_addr_in),        // input wire [27 : 0] din
.wr_en(wr_en_in),    // input wire wr_en
.rd_en(wr_cmd_en),    // input wire rd_en
.dout(wr_cmd_addr),      // output wire [27 : 0] dout
.full(),      // output wire full
.empty(wr_cmd_req)    // output wire empty
);

cmd_fifo rd_cmd_fifo (
.wr_clk(clk_125),  // input wire wr_clk\\tx模块用
//.wr_clk(ui_clk),  // input wire wr_clk\\
.rd_clk(ui_clk),  // input wire rd_clk
.din(rd_cmd_addr_in),        // input wire [27 : 0] din
.wr_en(rd_en_in),    // input wire wr_en
.rd_en(rd_cmd_en),    // input wire rd_en
.dout(rd_cmd_addr),      // output wire [27 : 0] dout
.full(),      	// output wire full
.empty(rd_cmd_req)    // output wire empty
);

data_fifo wr_data_fifo (
.wr_clk(clk_125),                // input wire wr_clk
.rd_clk(ui_clk),                // input wire rd_clk
.din(wr_data_in),                      // input wire [127 : 0] din
.wr_en(data_en_in),                  // input wire wr_en
.rd_en(data_req),                  // input wire rd_en
.dout(data_in),                    // output wire [127 : 0] dout
.full(),                    // output wire full
.empty(),                  // output wire empty
.rd_data_count(),  // output wire [6 : 0] rd_data_count
.wr_data_count()  // output wire [6 : 0] wr_data_count
);

data_fifo rd_data_fifo (
.wr_clk(ui_clk),                // input wire wr_clk
//.rd_clk(ui_clk),                // input wire rd_clk//////////////////////////这里花了很长时间
.rd_clk(clk_125),                // input wire rd_clk\\tx模块用
.din(app_rd_data_out),                      // input wire [127 : 0] din
.wr_en(app_rd_data_valid_out),                  // input wire wr_en
.rd_en(data_en_out),                  // input wire rd_en
.dout(rd_data_out),                    // output wire [127 : 0] dout
.full(),                    // output wire full
.empty(),                  // output wire empty
.rd_data_count(rd_data_count),  // output wire [6 : 0] rd_data_count
.wr_data_count()  // output wire [6 : 0] wr_data_count
);


/*
ila_1 ila_1_inst2 (
.clk(clk_200), // input wire clk
.probe0({41'b0,rd_data_count,rd_data_out[24:0],data_en_out,app_rd_data_valid_out,app_rd_data_out[24:0]}) // input wire [99:0] probe0
);	
*/

mid_ddr3 mid_ddr3_inst(
.sclk			(ui_clk		),
.rst_n			(init_calib_complete	),
.wr_cmd_req		(~wr_cmd_req		),//
.wr_cmd_en		(wr_cmd_en		),//
.wr_cmd_addr		(wr_cmd_addr		),//
.data_in		(data_in		),//
.data_req		(data_req		),//	
.rd_cmd_req		(~rd_cmd_req		),//
.rd_cmd_en		(rd_cmd_en		),//
.rd_cmd_addr		(rd_cmd_addr		),//
.app_rd_data_out	(app_rd_data_out	),//
.app_rd_data_valid_out	(app_rd_data_valid_out	),//
.app_addr		(app_addr		),//
.app_cmd		(app_cmd		),//
.app_en			(app_en			),//
.app_wdf_data		(app_wdf_data		),//
.app_wdf_end		(app_wdf_end		),//
.app_wdf_wren		(app_wdf_wren		),//
.app_wdf_mask		(app_wdf_mask		),//
.app_rd_data_valid	(app_rd_data_valid	),//
.app_rd_data		(app_rd_data		),
.app_rdy		(app_rdy		),
.app_wdf_rdy		(app_wdf_rdy		)
);
		
ddr3_ip	ddr3_ip_inst(
.sclk			(clk_200M		),
.rst_n			(rst_n			),
.ddr3_dq		(ddr3_dq		),
.ddr3_dqs_n		(ddr3_dqs_n		),
.ddr3_dqs_p		(ddr3_dqs_p		),
.ddr3_addr		(ddr3_addr		),
.ddr3_ba		(ddr3_ba		),
.ddr3_ras_n		(ddr3_ras_n		),
.ddr3_cas_n		(ddr3_cas_n		),
.ddr3_we_n		(ddr3_we_n		),
.ddr3_reset_n		(ddr3_reset_n		),
.ddr3_ck_p		(ddr3_ck_p		),
.ddr3_ck_n		(ddr3_ck_n		),
.ddr3_cke		(ddr3_cke		),
.ddr3_cs_n		(ddr3_cs_n		),
.ddr3_dm		(ddr3_dm		),
.ddr3_odt		(ddr3_odt		),
.app_addr		(app_addr		),
.app_cmd		(app_cmd		),
.app_en			(app_en			),
.app_wdf_data		(app_wdf_data		),
.app_wdf_end		(app_wdf_end		),
.app_wdf_mask		(app_wdf_mask		),
.app_wdf_wren		(app_wdf_wren		),
.app_rd_data		(app_rd_data		),
.app_rd_data_valid	(app_rd_data_valid	),
.app_rdy		(app_rdy		),
.app_wdf_rdy		(app_wdf_rdy		),     
.ui_clk			(ui_clk			),
.init_calib_complete    (init_calib_complete    )
);
/*
clk_200 clk_200_inst
(
// Clock out ports
.clk_200M(clk_200M),     // output clk_200M
// Clock in ports
.clk_50M(sclk)      // input clk_50M
);
*/
endmodule
