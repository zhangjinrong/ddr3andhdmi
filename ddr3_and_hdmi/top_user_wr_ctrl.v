module top_user_wr_ctrl(
input 	wire		sclk				,
input 	wire 		rst_n				,
input 	wire 		rx					,
//output 	wire 		tx	
output 	wire[27:0]	wr_addr	,
output 	wire		cmd_en			,
output 	wire[127:0]	wr_data	,
output	wire		data_en			
);

//wire[27:0]	wr_addr	;
//wire				cmd_en	;
//wire[127:0]	wr_data	;
//wire				data_en	;	

wire[7:0]	data_out;
wire 			en			;

ila_0 ila_0_rx (
.clk(sclk), // input wire clk
.probe0({41'b0,data_out,en,wr_data[23:0],data_en,wr_addr[23:0],cmd_en}) // input wire [99:0] probe0
);

rx rx_inst(
.clk					(sclk	),
.rst_n				(rst_n	),
.rx						(rx	),
.po_data_out	(data_out),
.en_out				(en	)
);
/*
tx tx_inst(
.data_in			(data_out),
.clk					(sclk	),
.rst_n				(rst_n	),
.en						(en	),
.tx						(tx	)
);
*/

user_wr_ctrl user_wr_ctrl_inst(
.sclk		(sclk	),
.rst_n		(rst_n	),
.en_in		(en	),
.pi_data_en	(data_out),
.wr_addr	(wr_addr),
.cmd_en		(cmd_en	),
.wr_data	(wr_data),
.data_en	(data_en)
);


endmodule
