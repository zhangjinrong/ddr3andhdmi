
module ddr3_read_ctrl(//////暂时没有问题
input 	wire 		sclk			,
input 	wire 		rst_n			,

input 	wire 		rd_en			,                                        	
input   wire[127:0]  app_rd_data_in		,
input   wire         app_rd_data_valid_in	,
input 	wire 		app_rdy			,
input 	wire[27:0] 	rd_cmd_addr		,//
input 	wire 		rd_cmd_req_in		,//

output  wire[127:0]     app_rd_data_out		,
output  wire          	app_rd_data_valid_out	, 
output 	reg[27:0]       rd_app_addr		,
output 	reg[2:0]        rd_app_cmd		,
output  reg        	rd_app_en		,
output  wire 		rd_cmd_req_out		,//
output  reg 		rd_cmd_en		//没有起到作用	
);

assign rd_cmd_req_out 	= rd_cmd_req_in;
assign app_rd_data_valid_out = app_rd_data_valid_in;
assign app_rd_data_out	= app_rd_data_in;

reg [7:0]	rd_cnt;

//rd_app_cmd
always @ (posedge sclk)
	if(rst_n == 0)
		rd_app_cmd <= 3'b1;

//rd_app_addr
always @ (posedge sclk)
	if(rst_n == 0)
		rd_app_addr<=0;
	else if(rd_en)
		rd_app_addr<=rd_cmd_addr;
	else if(rd_app_en&&app_rdy)
		rd_app_addr<=rd_app_addr+8;

//rd_cnt
always @ (posedge sclk)
	if(rst_n == 0 )
		rd_cnt <= 0;
	else if((rd_cnt == 63)&&(app_rdy==1))
		rd_cnt <= 0;
	else if(rd_app_en&&app_rdy)
		rd_cnt <=rd_cnt+1;

//rd_cmd_en
always @ (posedge sclk)
	if(rst_n == 0)
		rd_cmd_en <= 0 ;
	else 
		rd_cmd_en <= rd_en ;

//rd_app_en
always @ (posedge sclk)
	if(rst_n == 0)
		rd_app_en <= 0 ;
	else if(rd_en == 1)
		rd_app_en <= 1 ;
	else if((rd_cnt == 63)&&(app_rdy==1))
		rd_app_en<=0;

endmodule