module mid_ddr3(
input 	wire 		sclk			,
input 	wire 		rst_n			,
//----------------------------------write fifo
input	wire		wr_cmd_req		,//
output wire 		wr_cmd_en		,//
input 	wire[27:0] 	wr_cmd_addr		,//
input 	wire[127:0]	data_in		,//
output wire 		data_req		,//	
//----------------------------------read fifo
input 	wire 		rd_cmd_req		,//
output wire		rd_cmd_en		,//
input 	wire[27:0] 	rd_cmd_addr		,//
output 	wire[127:0]	app_rd_data_out		,//
output 	wire 		app_rd_data_valid_out	,//
//----------------------------------right
output 	wire[27:0]	app_addr		,//
output 	wire[2:0]	app_cmd			,//
output 	wire		app_en			,//
output 	wire[127:0]	app_wdf_data		,//
output 	wire		app_wdf_end		,//
output 	wire		app_wdf_wren		,//
output 	wire[15:0]	app_wdf_mask		,//
input 	wire 		app_rd_data_valid	,//
input 	wire[127:0]	app_rd_data		,
input 	wire 		app_rdy			,
input 	wire 		app_wdf_rdy		
);


wire 		wr_cmd_req_out		;
wire 		rd_cmd_req_out		;
wire[127:0] 	app_rd_data_in		;
wire 		app_rd_data_valid_in	;
wire[127:0]	app_wdf_data_in		;
wire 		app_wdf_end_in		;
wire 		app_wdf_wren_in		;
wire[15:0]	app_wdf_mask_in		;
wire 		wr_app_rdy		;
wire 		rd_app_rdy		;
wire		app_wdf_rdy_out		;
wire 		wr_en			;
wire 		rd_en			;
wire[27:0]	wr_app_addr		;
wire[2:0]	wr_app_cmd		;
wire 		wr_app_en		;
wire[27:0]	rd_app_addr		;
wire[2:0]	rd_app_cmd		;
wire 		rd_app_en		;

/*
ila_1 ila_1_inst3 (
.clk(sclk), // input wire clk
.probe0({data_in[23:0],app_wdf_data[24:0],app_addr[24:0],app_cmd,app_rd_data[24:0]})
);
*/

arbit arbit_inst(
.sclk			(sclk		),
.rst_n			(rst_n		),
//.------------------write----------------------------
.wr_en			(wr_en		),//
.app_wdf_rdy_out	(app_wdf_rdy_out),//
.wr_req			(wr_cmd_req_out	),//(empty)
.wr_app_rdy		(wr_app_rdy	),//
.wr_app_addr		(wr_app_addr	),//
.wr_app_cmd		(wr_app_cmd	),//
.wr_app_en		(wr_app_en	),//
.app_wdf_data_in	(app_wdf_data_in),//
.app_wdf_end_in		(app_wdf_end_in	),//
.app_wdf_mask_in	(app_wdf_mask_in),//
.app_wdf_wren_in	(app_wdf_wren_in),
//.-----------------read--------------------------------
.rd_en			(rd_en		),
.rd_app_rdy		(rd_app_rdy	),
.rd_req			(rd_cmd_req_out),//(empty)
.app_rd_data_out	(app_rd_data_in),
.app_rd_data_valid_out	(app_rd_data_valid_in),
.rd_app_addr		(rd_app_addr	),
.rd_app_cmd		(rd_app_cmd	),
.rd_app_en		(rd_app_en	),
//.-----------------right-----------------------------
.app_rd_data_valid	(app_rd_data_valid),
.app_rd_data		(app_rd_data	),
.app_rdy		(app_rdy	),
.app_wdf_rdy		(app_wdf_rdy	),
.app_wdf_data		(app_wdf_data	),
.app_wdf_end		(app_wdf_end	),
.app_wdf_wren		(app_wdf_wren	),
.app_wdf_mask		(app_wdf_mask	),
.app_addr		(app_addr	),
.app_cmd		(app_cmd	),
.app_en			(app_en		) 	
);

ddr3_read_ctrl ddr3_read_ctrl_inst(
.sclk			(sclk		),
.rst_n			(rst_n		),        	
.rd_en			(rd_en		),                                        	
.app_rd_data_in		(app_rd_data_in	),
.app_rd_data_valid_in	(app_rd_data_valid_in),
.app_rdy		(rd_app_rdy	),
.rd_cmd_addr		(rd_cmd_addr	),
.rd_cmd_req_in		(rd_cmd_req	),
.app_rd_data_out	(app_rd_data_out),
.app_rd_data_valid_out	(app_rd_data_valid_out), 
.rd_app_addr		(rd_app_addr	),
.rd_app_cmd		(rd_app_cmd	),
.rd_app_en		(rd_app_en	),
.rd_cmd_req_out		(rd_cmd_req_out	),
.rd_cmd_en		(rd_cmd_en	)		
);


ddr3_write_ctrl ddr3_write_ctrl_inst(
.sclk			(sclk		),
.rst_n			(rst_n		),
.wr_en			(wr_en		),//由arbit输入，表示可以写
.app_rdy		(wr_app_rdy	),
.app_wdf_rdy		(app_wdf_rdy_out),
.wr_cmd_req_in		(wr_cmd_req	),//fifo传进，有写需求,只起连接作用
.wr_cmd_addr		(wr_cmd_addr	),//fifo传进，写地址（64突发的首地址）
.wr_data		(data_in	),//fifo传进，写数据
.wr_cmd_en		(wr_cmd_en	),//输出到fifo，读命令
.data_req		(data_req	),//输出到fifo，读数据
.wr_app_addr		(wr_app_addr	),//输出写地址，64突发，每次+8
.wr_app_cmd		(wr_app_cmd	),//输出写命令，0
.wr_app_en		(wr_app_en	),//输出写使能，与app_rdy配合使用
.wr_cmd_req_out		(wr_cmd_req_out	),//传给arbit，只起连接作用
.app_wdf_data		(app_wdf_data_in),//输出写数据，主要起连接作用，不缓存
.app_wdf_end		(app_wdf_end_in	),//输出数据和app_wdf_wren配合使用
.app_wdf_mask		(app_wdf_mask_in),//输出掩码，为0
.app_wdf_wren		(app_wdf_wren_in) //输出数据和app_wdf_end配合使用
);

endmodule
