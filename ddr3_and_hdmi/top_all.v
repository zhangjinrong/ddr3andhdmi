
module top_all(
input 	wire 				sclk		,
input 	wire 				rst_n		,
//input 	wire 		rx			

input		wire			rx_clk		,
input 		wire[3:0]		rx_data	,
input		wire			rx_ctrl	,

output 	wire			phy_rst_n	,

output		wire[3:0]		tx_data	,
output		wire			tx_clk		,
output		wire			tx_ctrl	,

//----------------------ddr3
inout 		wire[15:0]       ddr3_dq		,
inout 		wire[1:0]        ddr3_dqs_n		,
inout 		wire[1:0]        ddr3_dqs_p		,
output 	wire[13:0]       ddr3_addr		,
output 	wire[2:0]        ddr3_ba		,
output  	wire             ddr3_ras_n		,
output  	wire             ddr3_cas_n		,
output  	wire             ddr3_we_n		,
output  	wire             ddr3_reset_n	,
output 	wire[0:0]        ddr3_ck_p		,
output 	wire[0:0]        ddr3_ck_n		,
output 	wire[0:0]        ddr3_cke		,
output 	wire[0:0]        ddr3_cs_n		,
output 	wire[1:0]        ddr3_dm		,
output 	wire[0:0]        ddr3_odt		
//----------------------hdmi
/*
output 	wire 		r_ser_data_p	,
output 	wire 		r_ser_data_n	,
output 	wire 		g_ser_data_p	,
output 	wire 		g_ser_data_n	,
output 	wire 		b_ser_data_p	,
output 	wire 		b_ser_data_n	,
output 	wire 		c_ser_data_p	,
output 	wire 		c_ser_data_n	,
output  	wire 		hdmi_en				
*/
);

wire[23:0]	data_fifo_hdmi	;
wire[127:0]	data_ddr3_fifo	;
wire 		en_fifo_ddr3	;
wire 		have_data;
wire		de;
wire 		po_start_flag;
wire[27:0] 	rd_cmd_addr_in;
wire 		rd_en_in;
wire 		init_calib_complete;
wire 		clk_65m;
wire		clk_325m;
wire 		clk_200M;

wire 		clk_50_1;
wire 		clk_50_2;

wire 		ui_clk	;
wire[6:0]	have_data1;
wire[6:0]	rd_data_count;

wire[27:0]	wr_cmd_addr_in	;
wire 		wr_en_in		;
wire[127:0]	wr_data_in		;
wire 		data_en_in		;

wire[127:0]	image_po_data	;	//拼接成功的128位
wire		image_po_en	;	
wire[23:0]	image_po_addr	;	
wire		image_po_cmd	;
wire		clk_125	;
wire		clk125_10	;

wire		tx_can_read	;
wire		hdmi_cen	;
wire		tx_en		;
wire		tx_end_read	;

wire[7:0]	tx_po_data;
wire		tx_po_en;
wire		pre_flag;

wire[7:0]	crc_data_out;
wire		crc_en_out;
wire[31:0]	mac_data;
/*
top_user_wr_ctrl top_user_wr_ctrl_inst(
.sclk			(clk_50_4	),
.rst_n		(rst_n		),
.rx				(rx				),      
.wr_addr	(wr_cmd_addr_in	),
.cmd_en		(wr_en_in	),
.wr_data	(wr_data_in	),
.data_en	(data_en_in	)
);
*/

clk50_125 clk50_125_inst
(
// Clock out ports
.clk_125(clk_125),     // output clk_125
.clk_200(clk_200M),     // output clk_200
.clk125_10(clk125_10),     // output clk125_10
// Clock in ports
.clk_50(clk_50_1));      // input clk_50

clk50_50 clk50_50_inst1
(
// Clock out ports
.clk50_out1(clk_50_1),     // output clk50_out1
.clk50_out2(clk_50_2),     // output clk50_out2
// Clock in ports
.clk50in(sclk));      // input clk50in
    
clk_65_235 clk_65_235_inst
(
// Clock out ports
.clk_65(clk_65m),     // output clk_65
.clk_325(clk_325m),     // output clk_325
// Clock in ports
.clk_50(clk_50_2));      // input clk_50

/*
ila_1 ila_1_inst1 (
.clk(ui_clk), // input wire clk
.probe0({data_ddr3_fifo[82:0],en_fifo_ddr3,init_calib_complete,rd_en_in,rd_cmd_addr_in[23:0]})//,app_rd_data_valid_out,app_rd_data_out[23:0]}) // input wire [99:0] probe0
);

ila_1 ila_1_inst2 (
.clk(clk_65m), // input wire clk
.probe0({75'b0,data_fifo_hdmi,de})//,app_rd_data_valid_out,app_rd_data_out[23:0]}) // input wire [99:0] probe0
);
*/

rx_rgmii_mac rx_rgmii_mac_inst(
.clk_125		(clk_125),	
.rst_n			(rst_n),
.rx_clk		(rx_clk),
.rx_ctrl		(rx_ctrl),
.rx_data		(rx_data),

.phy_rst_n		(phy_rst_n),           	
.image_po_data	(image_po_data),//image_Ctrl输出的
.image_po_en		(image_po_en),//image_Ctrl输出的
.image_po_addr	(image_po_addr),//image_Ctrl输出的
.image_po_cmd		(image_po_cmd) //image_Ctrl输出的
);
/*
mid_fifo mid_fifo_inst(
.clk_200	(ui_clk),
//.clk_200	(clk_65m),
.clk_65		(clk_65m),
//.rst_n		(rst_n),
.rst_n		(init_calib_complete),
//.data_in	(128'hffffff_ffffff_ffffff_ffffff_ffffff),
.data_in	(data_ddr3_fifo),
.have_data	(0),//(have_data1),//(~have_data),
.rd_data_count1	(rd_data_count),
//.rd_data_count1	(have_data1),
.de		(de),
.po_flag_start	(po_start_flag),
.data_out	(data_fifo_hdmi),
.ddr3_req	(en_fifo_ddr3),
.cmd_addr	(rd_cmd_addr_in),
.cmd_en2	(rd_en_in)
);
*/

/*
tx_mid_fifo tx_mid_fifo_inst(
.tx_en			(tx_en),
.clk_125		(clk_125),
.rst_n			(rst_n),
.tx_can_read		(tx_can_read),//ddr3存入数据，可以读
.ddr3_data_count	(rd_data_count),//ddr3数据fifo内有数，可以读
.ddr3_data		(data_ddr3_fifo),
.ddr3_data_en		(en_fifo_ddr3),
.ddr3_cmd		(rd_en_in),
.ddr3_addr		(rd_cmd_addr_in[23:0]),

.mac_can_read		(mac_can_read),//mid_fifo存有一定的数据，可以无缝供数\\一个时钟周期还是一直拉高？
.mac_data		(mac_data),
.mac_en		(mac_en),
.tx_end_read		(tx_end_read)//以太网已经全部写完，结束了。
);
*/

tx_mid_fifo2 tx_mid_fifo2_inst(
.clk_125		(clk_125),
.rst_n			(rst_n),
.tx_can_read		(tx_can_read),
.ddr3_data_count	(rd_data_count),
.ddr3_data		(data_ddr3_fifo),
.ddr3_data_en		(en_fifo_ddr3),
.ddr3_cmd		(rd_en_in),
.ddr3_cmd_addr	(rd_cmd_addr_in),

.mac_en		(mac_en),
.tx_end_read		(tx_end_read),
.mac_can_read		(mac_can_read),
.mac_data		(mac_data)	
);

arbit_midfifo arbit_midfifo_inst(
.clk		(clk_125),
.rst_n		(rst_n),
.wr_ddr3_addr	(image_po_addr),
.tx_end_read	(tx_end_read),

.tx_en 	(tx_en),
.hdmi_en	(hdmi_cen),//只是给hdmi一个工作使能
.tx_can_read	(tx_can_read)
);

ila_3 ila_3_inst1 (
	.clk(clk_125), // input wire clk
	.probe0({en_fifo_ddr3,mac_en,rd_data_count,image_po_addr,data_ddr3_fifo[92:0],mac_data}) // input wire [157:0] probe0
);

ila_2 ila_2_inst2 (
	.clk(clk_125), // input wire clk
	.probe0({tx_can_read,mac_can_read,rd_cmd_addr_in[17:0]}) // input wire [19:0] probe0
);

ila_2 ila_2_inst1 (
	.clk(clk_125), // input wire clk
	.probe0({11'b0,crc_en_out,crc_data_out}) // input wire [19:0] probe0
);

tx_ctrl2 tx_ctrl2_inst(
.can_read_flag(mac_can_read),
.clk_125(clk_125),
.rst_n(rst_n),
.pi_data(mac_data),
.phy_mac_rst_n(),

.req_data(mac_en),
.po_data(tx_po_data),
.po_en(tx_po_en),
.pre_flag(pre_flag),
.tx_end_read(tx_end_read)
);

crc32_d8_send_02 crc32_d8_send_02_inst(
.resetb(rst_n),
.sclk(clk_125),

.dsin(tx_po_en),
.din(tx_po_data),
.pre_flag(pre_flag),
.crc_err_en(0),

.dsout(crc_en_out),
.dout(crc_data_out)
);

the_oddr the_oddr_inst(
.pi_en			(crc_en_out),
.pi_data		(crc_data_out),
.clk_125		(clk_125),
.clk125_10		(clk125_10),
.rst_n			(rst_n),
.rx_data		(tx_data),
.rx_clk		(tx_clk),
.rx_ctrl 		(tx_ctrl)
);

ddr3_top ddr3_top_inst(
.clk_125	(clk_125),
.rst_n		(rst_n),
.clk_200M	(clk_200M),
.data_en_out	(en_fifo_ddr3),
.rd_data_out	(data_ddr3_fifo),
//.have_data	(have_data),
.rd_cmd_addr_in	(rd_cmd_addr_in	),
.rd_en_in	(rd_en_in),
.rd_data_count	(rd_data_count),
.wr_cmd_addr_in	(image_po_addr	),
.wr_en_in	(image_po_cmd	),
.wr_data_in	(image_po_data	),
.data_en_in	(image_po_en	),
.ddr3_dq	(ddr3_dq	),
.ddr3_dqs_n	(ddr3_dqs_n	),
.ddr3_dqs_p	(ddr3_dqs_p	),
.ddr3_addr	(ddr3_addr	),
.ddr3_ba	(ddr3_ba	),
.ddr3_ras_n	(ddr3_ras_n	),
.ddr3_cas_n	(ddr3_cas_n	),
.ddr3_we_n	(ddr3_we_n	),
.ddr3_reset_n	(ddr3_reset_n	),
.ddr3_ck_p	(ddr3_ck_p	),
.ddr3_ck_n	(ddr3_ck_n	),
.ddr3_cke	(ddr3_cke	),
.ddr3_cs_n	(ddr3_cs_n	),
.ddr3_dm	(ddr3_dm	),
.ddr3_odt	(ddr3_odt	),
.init_calib_complete	(init_calib_complete	),
.ui_clk		(ui_clk)
);
/*
vga_top vga_top_inst(
.clk_65			(clk_65m),	
.clk_325		(clk_325m),
//.rst_n			(rst_n),
//.data   		(24'h00ff00),
.rst_n		    (init_calib_complete),
.data 	      (data_fifo_hdmi),
.r_ser_data_p	(r_ser_data_p	        ),
.r_ser_data_n	(r_ser_data_n	        ),
.g_ser_data_p	(g_ser_data_p	        ),
.g_ser_data_n	(g_ser_data_n	        ),
.b_ser_data_p	(b_ser_data_p	        ),
.b_ser_data_n	(b_ser_data_n	        ),
.c_ser_data_p	(c_ser_data_p	        ),
.c_ser_data_n	(c_ser_data_n	        ),
.hdmi_en	(hdmi_en		 ),
.de		(de			 ),
.po_start_flag	(po_start_flag			)
//.clk_65				(clk_65m		)
);
*/
/*
gen_data gen_data_inst(
.sclk		(sclk		),
.rst_n		(init_calib_complete),
.wr_cmd_addr_in	(wr_cmd_addr_in	),//
.wr_en_in	(wr_en_in	),//
.rd_cmd_addr_in	(rd_cmd_addr_in	),//
.rd_en_in	(rd_en_in	),//
.wr_data_in	(wr_data_in	),//
.data_en_in	(data_en_in	)//
);
*/
/*
test_mid_fifo test_mid_fifo_inst(//用来测试HDMI的，成功
.sclk		(clk_65m),
.rst_n		(rst_n),
.cmd_en		(rd_en_in),
.have_data	(have_data1)
);
*/
endmodule
